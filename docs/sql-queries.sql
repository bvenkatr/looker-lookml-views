SELECT
    CASE
        WHEN age <= 18 THEN '18 and below'
        WHEN age > 18 AND age <= 25 THEN '19-25'
        WHEN age > 25 AND age <= 35 THEN '26-35'
        WHEN age > 35 AND age <= 45 THEN '36-45'
        WHEN age > 45 AND age <= 55 THEN '46-55'
        WHEN age > 55 AND age <= 65 THEN '56-65'
        WHEN age > 65 AND age <= 75 THEN '66-75'
        WHEN age > 75 AND age <= 85 THEN '76-85'
        WHEN age > 85 AND age <= 95 THEN '86-95'
        ELSE 'Above 95'
    END AS age_tier
FROM
    your_table_name;
